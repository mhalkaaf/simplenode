provider "aws" {
  region = "us-east-1"
  access_key = ""
  secret_key = ""
}

resource "aws_instance" "centos7" {
  ami = "ami-002070d43b0a4f171" 
  instance_type = "t2.micro"
  user_data = <<-EOF
                #!/bin/bash
                sudo yum install -y yum-utils
                sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
                sudo yum install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y
                sudo systemctl start docker
                sudo groupadd docker
                sudo usermod -aG docker $USER
                sudo chmod 666 /var/run/docker.sock
                EOF
  tags = {
    Name = "Centos7"
  }
}

  